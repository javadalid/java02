package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase de arreglos, contiene todos los m�todos a implementar: Obtiene numero
 * mayor, menor, repetidos, separa true y false, Busca palabras clave y realiza
 * cambios en una lista.
 * 
 * @author Javier Adalid
 *
 */
public class Arreglos {

	private static final Logger LOGGER = Logger.getLogger(Arreglos.class.getName());

	/**
	 * Obtiene el numero mayor de un arreglo.
	 * 
	 * @param arreglo: arreglo de ints
	 * @return mayor: devuelve el numero mayor type int
	 */
	public static int numMayor(int[] arreglo) {
		int mayor = arreglo[0];
		try {
			for (int numero : arreglo) {
				if (numero > mayor)
					mayor = numero;
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}
		return mayor;
	}

	/**
	 * Obtiene el numero menor de un arreglo.
	 * 
	 * @param arreglo: arreglo de ints
	 * @return menor: devuelve el numero menor type int
	 */
	public static int numMenor(int[] arreglo) {
		int menor = arreglo[0];
		try {
			for (int numero : arreglo) {
				if (numero < menor)
					menor = numero;
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}

		return menor;
	}

	/**
	 * M�todo que devuelve un arreglo con los n�meros repetidos que se encuentran
	 * dentro de otro arreglo.
	 * 
	 * @param arreglo: arreglo de ints
	 * @return newArr: un arreglo de ints con todos los numeros repetidos
	 */
	public static String repetidos(int[] arreglo) {

		ArrayList<Integer> newArr = new ArrayList<Integer>();
		try {
			for (int i = 0; i < arreglo.length; i++) {
				for (int j = i + 1; j < arreglo.length; j++) {
					if (arreglo[i] == (arreglo[j])) {
						if (!newArr.contains(arreglo[i]))
							newArr.add(arreglo[i]);
					}

				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}

		return "Los numeros repetidos son: " + newArr.toString();
	}

	/**
	 * M�todo que separa un array de booleanos en dos arrays distintos.
	 * 
	 * @param arreglo: Un array de booleanos true y false
	 * @return separados: Un Array que contiene dos Arrays, uno con los trues y otro
	 *         con los falses
	 */
	public static String separaBool(boolean[] arreglo) {
		ArrayList<ArrayList<Boolean>> separados = new ArrayList<ArrayList<Boolean>>();
		ArrayList<Boolean> trues = new ArrayList<Boolean>();
		ArrayList<Boolean> falses = new ArrayList<Boolean>();
		try {
			for (int i = 0; i < arreglo.length; i++) {
				if (arreglo[i] == true)
					trues.add(arreglo[i]);
				else
					falses.add(arreglo[i]);
			}

			separados.add(trues);
			separados.add(falses);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}

		return separados.get(0).toString() + separados.get(1).toString();

	}

	/**
	 * M�todo que encuentra un string buscado dentro de un arreglo de strings.
	 * 
	 * @param palabra: palabra a buscar en el array, type string
	 * @param arreglo: arreglo de strings en el que se buscar� la palabra.
	 * @return res: Leyenda si fue encontrada o no, y en qu� posici�n del array.
	 */
	public static String encuentraPalabra(String palabra, String[] arreglo) {
		boolean encontrado = false;
		int index = 0;
		String res;
		try {
			for (int i = 0; i < arreglo.length; i++) {
				if (arreglo[i] == palabra) {
					encontrado = true;
					index = i;
					break;
				}
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}
		if (!encontrado)
			res = "No se encontr� la palabra: '" + palabra + "' dentro del array";
		else
			res = "Se encontr� la palabra: '" + palabra + "' dentro del array, en la posicion: " + index;

		return res;
	}

}
