package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase curso, contiene sus atributos: fecha y nombre tipo string M�todos
 * ordenar por nombre y remover objeto.
 * 
 * @author Javier Adalid
 *
 */
public class Curso {

	private static final Logger LOGGER = Logger.getLogger(Arreglos.class.getName());

	public String nombre;
	public String fecha;
	static ArrayList<Curso> cursos = new ArrayList<Curso>();

	/**
	 * Constructor de un curso.
	 * 
	 * @param nombre: tipo string
	 * @param fecha:  tipo string
	 */
	public Curso(String nombre, String fecha) {
		super();
		this.nombre = nombre;
		this.fecha = fecha;
		// Se a�ade al Arraylist de cursos el curso creado como objeto
		cursos.add(this);

	}

	/**
	 * Obtiene el nombre
	 * 
	 * @return: nombre del curso como string.
	 */
	public String getNombre() {
		return nombre.toLowerCase();
	}

	/**
	 * Inserta el nombre al curso.
	 * 
	 * @param nombre como string.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene la fecha
	 * 
	 * @return: fecha del curso como string.
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Inserta la fecha al curso.
	 * 
	 * @param fecha como string.
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * Remueve un curso de la lista de cursos
	 * 
	 * @param nombre: nombre del curso como string (no hace distinci�n con
	 *                may�sculas y min�sculas)
	 * @return: imprime la lista de cursos actualizada.
	 */
	public static void remover(String nombre) {
		try {
			// bandera para comprobar si fue encontrado
			boolean encontrado = false;
			// Se recorre el array de cursos
			for (Curso curso : cursos) {
				// Remueve el curso si su nombre es igual al provisto
				if (curso.getNombre() == nombre.toLowerCase()) {
					cursos.remove(curso);
					LOGGER.info("El curso: " + nombre + " ha sido borrado.");
					// Cambia la bandera a true
					encontrado = true;

					// Se sale del loop para evitar que siga recorriendo
					break;
				}
			}
			// Si no se encontr�, despliega el log.
			if (!encontrado) {
				LOGGER.warning("El curso: " + nombre + " no se encontr�.");
			}
			// Si se encontr�, imprime los cursos actualizados.
			else {
				System.out.println("Lista de cursos actualizada: ");
				for (Curso cu : cursos)
					System.out.println(cu.getNombre());
			}
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}

	}

	/**
	 * M�todo que ordena por nombre los cursos creados (objetos). Imprime la lista
	 * con los cursos ordenados.
	 */
	public static void ordenarPorNombre() {
		try {
			// temporal para comparaci�n
			String temp;
			// Creaci�n de ArrayList de tipo string
			ArrayList<String> strings = new ArrayList<String>();
			// Variable para imprimir la lista final ordenada
			int x = 0;
			// Loop que recorre los cursos (objetos)
			for (Curso c : cursos) {
				// Se a�ade el nombre del curso al ArrayList
				strings.add(c.getNombre().toLowerCase().toString());
			}
			// strings se convierte a Array para manejo de comparador
			Object[] str = strings.toArray();
			// Se recorre el array de str para comparar nombres. Orden alfab�tico.
			for (int i = 0; i < str.length; i++) {
				// Segundo loop para comparar el nombre siguiente
				for (int j = i + 1; j < str.length; j++) {
					if (str[i].toString().compareTo(str[j].toString()) > 0) {
						temp = str[i].toString();
						str[i] = str[j];
						str[j] = temp;
					}
				}
			}
			// Se recorre el array final y se imprime ordenado.
			System.out.println("Cursos ordenados alfab�ticamente: ");
			while (x < str.length) {
				System.out.println(str[x].toString());
				x++;
			}

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� una excepci�n", ex);
		}
	}
}
