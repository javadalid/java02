package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.array.Curso;
import deloitte.academy.lesson01.array.Arreglos;

/**
 * Clase Main en donde se ejecutan y prueban todos los m�todos de la clase
 * Arreglos y Curso
 * 
 * @author Javier Adalid
 *
 */
public class Main {

	public static void main(String[] args) {
		// Se crea un array de ints para prueba
		int[] array = { 1, 2, 3, 1, 2, 3, 4, 5, 9, 9 };
		// Se crea un array de bools para prueba
		boolean[] arrayBool = { true, false, true, true, false, false, false, true };
		// Se crea un array de strings para prueba
		String[] arrayStrings = { "hola", "adios", "prueba", "test", "calidad", "software" };
		// Creaci�n de objetos de tipo Curso
		Curso curso1 = new Curso("zuckerberg", "12/12/2000");
		Curso curso2 = new Curso("analisis", "12/04/1997");
		Curso curso3 = new Curso("bitcoins", "11/12/2007");

		/**
		 * Prueba del numero mayor
		 */
		System.out.println(Arreglos.numMayor(array));

		/**
		 * Prueba del n�mero menor
		 */
		System.out.println(Arreglos.numMenor(array));

		/**
		 * Prueba de los numeros repetidos
		 */
		System.out.println(Arreglos.repetidos(array));

		/**
		 * Prueba de los arrays de booleanos
		 */
		System.out.println(Arreglos.separaBool(arrayBool));

		/**
		 * Prueba de la b�squeda de palabras
		 */
		System.out.println(Arreglos.encuentraPalabra("test", arrayStrings));

		/**
		 * Prueba de remoci�n de un curso por su nombre
		 */
		Curso.remover("analisis");

		/**
		 * Prueba de ordenamiento de cursos por nombre
		 */
		Curso.ordenarPorNombre();

	}

}
